package br.com.dev.crud.usuario;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/usuario")
public interface UsuarioRest {

    @PostMapping("/user")
    public Usuario salvar(@RequestBody Usuario Usuario);

    @GetMapping("/consultar/{idUsuario}")
    public Usuario consultar(@PathVariable("idUsuario") Long idUsuario);

    @GetMapping("/deletar/{idUsuario}")
    public void deletar(@PathVariable("idUsuario") Long idUsuario);

}
