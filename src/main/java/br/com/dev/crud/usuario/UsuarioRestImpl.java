package br.com.dev.crud.usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioRestImpl implements UsuarioRest {

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public Usuario salvar(@RequestBody Usuario Usuario) {
        return usuarioService.salvarUsuario(Usuario);
    }

    @Override
    public Usuario consultar(@PathVariable Long idUsuario) {
        return usuarioService.consultarUsuario(idUsuario);
    }

    @Override
    public void deletar(@PathVariable Long idUsuario) {
        usuarioService.deletarUsuario(idUsuario);
    }

}
