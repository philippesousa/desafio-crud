package br.com.dev.crud.usuario;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.dev.crud.endereco.Endereco;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Usuario {
    @Id
    @Column(name = "ID_USUARIO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUsuario;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "CPF")
    private String cpf;

    @Column(name = "EMAIL_PESSOAL")
    private String emailPessoal;

    @JoinColumn(name = "ID_ENDERECO")
    @OneToOne
    private Endereco endereco;

    @Column(name = "EMAIL_COMERCIAL")
    private String emailComercial;

    @Column(name = "TEL_CELULAR")
    private String telCelular;

    @Column(name = "TEL_COMERCIAL")
    private String telComercial;

    @Column(name = "TEL_RESIDENCIAL")
    private String telResidencial;

}
