package br.com.dev.crud.usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dev.crud.endereco.Endereco;
import br.com.dev.crud.endereco.EnderecoService;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private EnderecoService enderecoService;

    public Usuario salvarUsuario(Usuario usuario) {
        Endereco endereco = usuario.getEndereco();
        enderecoService.salvar(endereco);
        return usuarioRepository.save(usuario);
    }

    @Transactional(readOnly = true)
    public Usuario consultarUsuario(Long idUsuario) {
        return usuarioRepository.findById(idUsuario).get();
    }

    public void deletarUsuario(Long idUsuario) {
        usuarioRepository.deleteById(idUsuario);
    }

}
