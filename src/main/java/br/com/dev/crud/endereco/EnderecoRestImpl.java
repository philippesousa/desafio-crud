package br.com.dev.crud.endereco;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EnderecoRestImpl implements EnderecoRest {

    @Autowired
    private EnderecoService enderecoService;

    @Override
    public Endereco salvarEndereco(@RequestBody Endereco endereco) {
        return enderecoService.salvar(endereco);
    }

    @Override
    public Endereco getCep(String cep) {
        return enderecoService.getCep(cep);
    }
}
