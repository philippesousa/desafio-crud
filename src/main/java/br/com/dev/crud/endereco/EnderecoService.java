package br.com.dev.crud.endereco;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EnderecoService {

    @Autowired
    private EnderecoRepository enderecoRepository;

    public Endereco salvar(Endereco endereco) {
        validadeDados(endereco);
        return enderecoRepository.save(endereco);
    }

    public Endereco getCep(String cep) {
        String cepTratado = cep.replace(".", "").replace("-", "");
        RestTemplate restTemplate = new RestTemplate();
        Endereco endereco = restTemplate.getForObject("http://viacep.com.br/ws/" + cepTratado + "/json/unicode/", Endereco.class);
        return endereco;
    }

    private void validadeDados(Endereco endereco) {
        try {
            if (endereco.getCep() == null || endereco.getLogradouro() == null || endereco.getBairro() == null
                    || endereco.getCidade() == null || endereco.getUf() == null) {
                throw new Exception("Campos Obrigatórios não foram preenchidos preenchidos");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
